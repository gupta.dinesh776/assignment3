import React, { useState } from "react";

const SearchBar=({thisIsAnotherCallBack})=>{
  const [term,setTerm]=useState('');
  const [term2,setTerm2]=useState('')


 const onFormSubmit = async (event) => {
    event.preventDefault();
    setTimeout(() => {
      thisIsAnotherCallBack(term,term2);
    }, 1000);
  };

  return (
    <div className="search-panel" style={{ marginTop: "20px" }}>
      <form onSubmit={onFormSubmit}>
        <span>
          <label htmlFor="search-what">What</label>
          <input
            onChange={(event)=>setTerm(event.target.value)}
            id="search-what"
            className="ui input focus active"
            type="text"
            placeholder="Job title, keywords or company"
          />
        </span>
        <span style={{ marginLeft: "20px" }}>
          <label htmlFor="search-where"> Where</label>
          <input
            onChange={(event)=>setTerm2(event.target.value)}
            type="text"
            placeholder="Enter city"
          />
        </span>
        <span style={{ marginLeft: "20px" }}>
          <button style={{ color: "blue" }}>Find jobs</button>
        </span>
      </form>
    </div>
  );

}

export default SearchBar;
