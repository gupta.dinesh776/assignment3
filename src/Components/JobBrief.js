import React from "react";

const JoibBrief = ({ job, thisIsaCallBack }) => {
  return (
    <div
      onClick={() => thisIsaCallBack(job)}
      className=""
      style={{ maxWidth: "20vw", cursor: "pointer" }}
    >
      <h4>{job.name}</h4>
      <h5>
        {job.location.city},{job.location.country}
      </h5>
      <img className="job-post-image" src={job.logo} alt="companylogo" />
      <p>{job.description}</p>
      <div className="salary">RS: {job.salary}Montly</div>
      <button style={{ backgroundColor: "blue", color: "white" }}>Apply</button>
      <button style={{ backgroundColor: "red", color: "white" }}>
        Not interested
      </button>
      <hr />
    </div>
  );
};

export default JoibBrief;
