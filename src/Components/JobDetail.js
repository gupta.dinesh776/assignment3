import React from "react";

const JobDetail = ({ job }) => {
  if (!job) {
    return <div></div>;
  }
  return (
    <div className="ui placeholder segment">
      <h4 className="ui header">{job.name}</h4>
      <h5>
        {job.location.city},{job.location.country}
      </h5>
      <img className="job-post-image" src={job.logo} alt="companylogo" />
      <p className="ui header">{job.description}</p>
      <div className="ui header">RS: {job.salary}Montly</div>
      <div className="inline">
        <button className="ui primary button">Apply</button>
        <button className="ui red button">Not interested</button>
      </div>
    </div>
  );
};

export default JobDetail;
