import React, { useState } from "react";
import PersonalInfo from "../Profile/PersonalInfo";
import WorkExperience from "../Profile/WorkExperience";
import Education from "../Profile/Education";
import Skills from "../Profile/Skills";
import Toggle from "../Profile/Toggle";


const Profile = () => {
  const [open, setopen] = useState(true);
  const [openEdu,setOpenEdu]=useState(false);
  const[workexperience,setWorkExperince]=useState([])

  return (
    <div className="ui container">
      <PersonalInfo />
      {open ? (
        <WorkExperience
     getdatacallBack={setWorkExperince}
        gettoggleanswer={setopen} 
       /> ) :(<Toggle
      label="Work-Experience"
      thisiscallback={setopen}
      />)}
     
     {openEdu ?(<Education 
      gettoggleanswer={setOpenEdu}
     
      />):<Toggle
      label="Education"
      thisiscallback={setOpenEdu}
      />}
      <Skills />
    </div>
  );
};
export default Profile;
