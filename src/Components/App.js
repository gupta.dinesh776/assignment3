import React, { useEffect, useState } from "react";
import JobWebsite from "./JobWebsite";
import SearchBar from "./SearchBar";
import Jobs from "../jobs.json";
import JobList from "./JobList";
import JobDetail from "./JobDetail";

const App =()=>{
  const [jobs,setJobs]=useState(Jobs);
  const [isLoading,setisLoading]=useState('true')
  const [onJobSelect,setOnJobSelect]=useState(null);
  const [name,setName]=useState('')
  const [city,setCity]=useState('')

  useEffect(()=>{
    setTimeout(() => {
      setisLoading('false')
    }, 1000);
  },[])
 

 const onTermSubmit = async (term, term2) => {
   setName(term);
   setCity(term2);
  };

const rendercondition=()=> {
    const filtermethod = jobs.filter((job) => {
      if (
        job.name.includes(name) &&
        job.location.city.includes(city)
      ) {
        return job.name.includes(name);
      }
    });
    if (isLoading === "true") {
      return (
        <div
          style={{ textAlign: "center", fontSize: "30px", marginTop: "10px" }}
        >
          Loading...
        </div>
      );
    } else {
      return (
        <div className="" style={{ padding: "30px" }}>
          <JobWebsite />
          <SearchBar thisIsAnotherCallBack={onTermSubmit} />
          <div className="ui grid" style={{ marginTop: "20px" }}>
            <div className="row">
              <div className="five wide column">
                <JobList
                  onSelectWhenClicked={setOnJobSelect}
                  jobs={filtermethod}
                />
              </div>
              <div className="ten wide column">
                <JobDetail job={onJobSelect} />
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
  return <div>{rendercondition()}</div>;

}


export default App;
