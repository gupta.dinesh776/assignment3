import React from "react";
import JoibBrief from "./JobBrief";

const JobList = ({ jobs, onSelectWhenClicked }) => {
  const renderedList = jobs.map((job) => {
    return (
      <JoibBrief job={job} key={job.id} thisIsaCallBack={onSelectWhenClicked} />
    );
  });
  return <div>{renderedList}</div>;
};

export default JobList;
