import React from "react";

class JobWebsite extends React.Component {
  render() {
    return (
      <div>
        <div className="main-heading">
          <div className="company-name">
            <h2>MentorStudents Job-Portal</h2>
            <img
              src={"https://picsum.photos/id/180/500/200"}
              alt="banner"
              className="company-banner"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default JobWebsite;
