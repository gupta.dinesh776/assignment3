import React, { useEffect, useState } from "react";
import Dropdown from "../Profile/Dropdown";
import month from "../month.json";
import year from "../year.json";

const WorkExperience = ({ gettoggleanswer ,getdatacallBack}) => {
  const [opentoggle, setopentoggle] = useState(true);
  const [selected, setSelected] = useState(month[0]);
  const [yearselect, setyearSelect] = useState(year[0]);
  const [endMonth, setEndMonth] = useState(month[0]);
  const [endyear, setEndYear] = useState(year[0]);
  const[data,setData]=useState({
      title:'',
      company:'',
      location:'',
  })

//   useEffect(()=>{
    
//   },[data])
  const onHandleSubmit = (event) => {
    event.preventDefault();
    const {name,value}=event.target;
    setData({...data,[name]:value})
    getdatacallBack(data);
  };

  return (
    <div>
      <form onSubmit={onHandleSubmit} className="ui form">
        <div className="fields">
          <h4 className="ui header eight wide field">Work Experience</h4>
          <button
            onClick={() => gettoggleanswer(!opentoggle)}
            className="ui primary button"
            type="button"
          >
            Close
          </button>
        </div>
        <div className="field">
          <label>Job Title</label>
          <input
           
            name="title"
            onChange={onHandleSubmit}
            name="Job-title"
            type="text"
          />
        </div>
        <div className="field">
          <label>Company Name </label>
          <input
          name="company"
            type="text"
            
            onChange={onHandleSubmit}
          />
        </div>
        <div className="field">
          <label>Location</label>
          <input
          name="location"
            type="text"
           
            onChange={onHandleSubmit}
          />
        </div>

        <label>Time-Period</label>

        <div className="fields">
          <div className="four wide field">
            <Dropdown
              selected={selected}
              onSelected={setSelected}
              items={month}
              label="Start Month"
            />
          </div>

          <div className="four wide field">
            <Dropdown
              selected={yearselect}
              onSelected={setyearSelect}
              items={year}
              label="Start Year"
            />
          </div>
          <div className="four wide field">
            <Dropdown
              selected={endMonth}
              onSelected={setEndMonth}
              items={month}
              label="End Month"
            />
          </div>

          <div className="four wide field">
            <Dropdown
              selected={endyear}
              onSelected={setEndYear}
              items={year}
              label="End Year"
            />
          </div>
        </div>
        <button className="ui primary button" type="submit">
          Save
        </button>
      </form>
    </div>
  );
};

export default WorkExperience;
