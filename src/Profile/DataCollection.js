import React, { useEffect, useState } from "react";

const DataCollection = ({items}) => {
    console.log(items)
    const renderedList=items.map((item)=>{
        if(item===null){
            return null;
        }
        return(<div className="ui form">
      <div className="fields">
        <div class="ui message ten wide field">
          <div class="header">{item.title}</div>
          <p>{item.company}</p>
          <p>{item.location}</p>
          <p>July2018 to Jan 2019 </p>
        </div>
        <div className="field">
          <button 
          
          className="ui red button">Remove</button>
        </div>
      </div>
    </div>)
    })
  return (
      <div>
          {renderedList}
      </div>
    
  );
};

export default DataCollection;
