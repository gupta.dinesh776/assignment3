import React from 'react'

const PersonalInfo=()=>{
    return (<div>
<form className="ui form">
    <h4 className="ui dividing header">Personal Information</h4>
    <div className="fields">
        <div className="six wide field">
        <label>FirstName</label>
        <input type="text" placeholder="First Name"></input>
        </div>
        <div className="six wide field">
            <label>Last Name</label>
            <input type="text" placeholder="Last Name"/>
        </div>
    </div>
    <div className="fields">
        <div className="six wide field">
        <label>Email-ID</label>
        <input type="text" placeholder="email-id"></input>
        </div>
        <div className="six wide field">
            <label>Mobile No.</label>
            <input type="text" placeholder="Mobile Number"/>
        </div>
    </div>
    <div className="fields">
        <div className="six wide field">
        <label>City</label>
        <input type="text" placeholder="City"></input>
        </div>
        <div className="six wide field">
            <label>Postal-Code</label>
            <input type="text" placeholder="Pincode"/>
        </div>
    </div>
    <div className="fields">
        <div className="eight wide field">
    <label>Describe your position and accomplishments as Full-Stack Developer</label>
    <textarea rows="2"  ></textarea>
    </div>
  </div>
  <button className="ui primary button" type="submit">Save</button>
</form>

    </div>);
}
export default PersonalInfo;