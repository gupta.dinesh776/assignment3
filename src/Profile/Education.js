import React, { useState } from "react";
import Dropdown from "../Profile/Dropdown";
import month from "../month.json";
import year from "../year.json";
const Education = ({gettoggleanswer}) => {
    const[opentoggle,setopentoggle]=useState(true);
  const [selected, setSelected] = useState(month[0]);
  const [yearselect, setyearSelect] = useState(year[0]);
  const [endMonth, setEndMonth] = useState(month[0]);
  const [endyear, setEndYear] = useState(year[0]);
  const [term,setTerm]=useState('')

const onInputSubmit=(event)=>{
   
    const data=event.target.value;
    setTerm(data)
    console.log(data)
}

  return (
    <div>
      <form className="ui form">
        <div className="fields">
          <h4 className="ui header eight wide field">Education</h4>
          <button className="ui primary button" type="button"
           onClick={()=>gettoggleanswer(!opentoggle)}>
            Close
          </button>
        </div>
        <div className="field">
          <label>Degree</label>
          <input
          onChange={onInputSubmit}
          type="text" />
        </div>
        <div className="field">
          <label>College</label>
          <input type="text" />
        </div>
        <div className="field">
          <label>Location</label>
          <input type="text" />
        </div>

        <label>Time-Period</label>

        <div className="fields">
          <div className="four wide field">
            <Dropdown
              selected={selected}
              onSelected={setSelected}
              items={month}
              label="Start Month"
            />
          </div>

          <div className="four wide field">
            <Dropdown
              selected={yearselect}
              onSelected={setyearSelect}
              items={year}
              label="Start Year"
            />
          </div>
          <div className="four wide field">
            <Dropdown
              selected={endMonth}
              onSelected={setEndMonth}
              items={month}
              label="End Month"
            />
          </div>

          <div className="four wide field">
            <Dropdown
              selected={endyear}
              onSelected={setEndYear}
              items={year}
              label="End Year"
            />
          </div>
        </div>
        <button 
        
        className="ui primary button" type="submit">
          Save
        </button>
      </form>
    </div>
  );
};

export default Education;
