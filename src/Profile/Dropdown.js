import React, { useEffect, useRef, useState } from 'react';

const Dropdown=({items,label,selected,onSelected})=>{
    const [open,setOpen]=useState(false);
const[getvalue,setvalue]=useState(items[0].label)
const ref=useRef();
    useEffect(()=>{
        const BodyListner=(event)=>{
            if(ref.current&& ref.current.contains(event.target)){
                return;
            }
            setOpen(false)
        };
        document.body.addEventListener('click',BodyListner)
        return()=>{
            document.body.removeEventListener('click',BodyListner)
        }
    },[])
  
    const rendereditems=items.map((item)=>{
        if(item.value === selected.value){
            return null;
        }
return (
    <div
    onClick={()=>onSelected(item)}
    key={item.value} className="item">
        {item.label}
    </div>
)
    })
    return (
        <div ref={ref} className="ui form"
        >
            <div className="field">
                <label className="label">{label} </label>
                <div
                onClick={()=>setOpen(!open)}
                className={`ui selection dropdown ${open?'visible active':''}`}>
                    <i className="dropdown icon"></i>
                    <div className="text"
                  
                    >{selected.label}</div>
                    <div
                     className={`menu ${open?'visible transition':''}`}>
                        {rendereditems}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Dropdown;