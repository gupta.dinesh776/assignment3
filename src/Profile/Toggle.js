import React, { useState } from 'react';

const Toggle =({label,thisiscallback})=>{
    const [toggle,setToggle]=useState(true);
return (
    <div className="ui form">
      <div className="fields">
        <div className="ui header eight wide field">{label}</div>
        <div className="">
          <button
            className="ui primary button "
            type="button"
            onClick={() =>{ thisiscallback(toggle)}}
          >
            ADD
          </button>
        </div>
      </div>
    </div>
  )
}

export default Toggle;